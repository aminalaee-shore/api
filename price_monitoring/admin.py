from django.contrib import admin

from .models import Alert, AlertLog, Product, ProductLog


@admin.register(Alert)
class AlertAdmin(admin.ModelAdmin):
    list_display = ['email', 'interval_minutes', 'product_category']
    list_filter = ['interval_minutes']


@admin.register(AlertLog)
class AlertLogAdmin(admin.ModelAdmin):
    list_display = ['alert', 'created_at', 'sent_at']


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['category', 'name', 'sku']


@admin.register(ProductLog)
class ProductLogAdmin(admin.ModelAdmin):
    list_display = ['price', 'product']
