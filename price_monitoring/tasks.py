import json
from datetime import datetime
from typing import Any, Dict

import dramatiq
import requests
from django.core.mail import send_mail
from django.db import models
from django.dispatch import receiver

from api.event_publisher import event_publisher
from .models import Alert, AlertLog, Product


@dramatiq.actor
def email_customer(alert_id, message):
    alert = Alert.objects.get(pk=alert_id)
    log = AlertLog.objects.create(alert=alert)

    # Retry inside dramatiq
    send_mail("Price Monitoring Alert", message, "info@example.com", [alert.email])

    log.sent_at = datetime.now()
    log.save()


@dramatiq.actor
def publish_product_data(data: Dict[str, Any]):
    event_publisher.add(json.dumps(data))


@dramatiq.actor
def get_ebay_prices(alert_id):
    alert: Alert = Alert.objects.get(pk=alert_id)

    ebay_response = requests.get("/ebay").json()
    for response in ebay_response:
        product = Product(**response)
        product.save()

        publish_product_data.send(response)

    email_customer.send(args=[alert.pk, ebay_response])

    get_ebay_prices.send_with_options(args=[alert.pk], delay=alert.interval_minutes * 60 * 1000)


@receiver(models.signals.post_save, sender=Alert)
def post_alert_subscription(sender, instance: Alert, created: bool, **kwargs):  # pylint: disable=unused-argument
    if not created:
        return  # Not sure what is the expected behaviour

    get_ebay_prices.send_with_options(
        args=[instance.pk], delay=instance.interval_minutes * 60 * 1000)
