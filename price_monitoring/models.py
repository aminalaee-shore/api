from django.db import models


class Alert(models.Model):
    class IntervalMinutes(models.IntegerChoices):
        TWO_MINUTES = 2
        FIVE_MINUTES = 5
        THIRTY_MINUTES = 30

    email = models.EmailField()
    interval_minutes = models.IntegerField(
        choices=IntervalMinutes.choices, default=IntervalMinutes.FIVE_MINUTES)
    product_category = models.CharField(max_length=64, blank=True, null=True)

    def __repr__(self):
        return str(self.pk)


class AlertLog(models.Model):
    alert = models.ForeignKey(Alert, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    sent_at = models.DateTimeField(blank=True, null=True)

    def __repr__(self):
        return str(self.pk)


class Product(models.Model):
    category = models.CharField(max_length=32)
    name = models.CharField(max_length=256)
    sku = models.CharField(max_length=32)

    def __repr__(self):
        return str(self.pk)


class ProductLog(models.Model):
    price = models.DecimalField(max_digits=9, decimal_places=2)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __repr__(self):
        return str(self.pk)
