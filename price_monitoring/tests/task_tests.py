from django.core import mail

from price_monitoring.models import Alert
from price_monitoring.tasks import email_customer


def test_sending_emails(broker, worker):
    alert = Alert.objects.create(email="mohammadamin.alaee@gmail.com")

    email_customer(alert.pk, "Price Monitoring Alert")

    assert len(mail.outbox) == 1
    assert mail.outbox[0].subject == "Price Monitoring Alert"


def test_getting_ebay_prices():
    alert = Alert(email="mohammadamin.alaee@gmail.com")
    alert.save()

