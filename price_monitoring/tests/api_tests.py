from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class EbayStubTests(APITestCase):
    def test_get_ebay_data(self):
        url = reverse('product-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.json())


class AlertTests(APITestCase):
    def test_list_alerts(self):
        url = reverse('alert-list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.json())

    def test_create_alerts(self):
        url = reverse('alert-list')
        data = {"email": "mohammadamin.alaee@gmail.com"}
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(response.json())
