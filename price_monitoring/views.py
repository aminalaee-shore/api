from model_bakery import baker
from rest_framework import viewsets
from rest_framework.response import Response

from .models import Alert, AlertLog, Product, ProductLog
from .serializers import (AlertLogSerializer, AlertSerializer,
                          ProductLogSerializer, ProductSerializer)


class AlertViewset(viewsets.ModelViewSet):
    queryset = Alert.objects.order_by('id')
    serializer_class = AlertSerializer


class AlertLogViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AlertLog.objects.order_by('-created_at')
    serializer_class = AlertLogSerializer


class EbayStubViewset(viewsets.ViewSet):
    """
    Since Ebay suspended my accounts, this stub view will mock ebay calls
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def list(self, request):  # pylint: disable=unused-argument
        products = baker.prepare('Product', _quantity=20)
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)


class ProductViewset(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.order_by('id')
    serializer_class = ProductSerializer


class ProductLogViewset(viewsets.ReadOnlyModelViewSet):
    queryset = ProductLog.objects.order_by('id')
    serializer_class = ProductLogSerializer
