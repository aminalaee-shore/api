from django.apps import AppConfig


class PriceMonitoringConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'price_monitoring'
