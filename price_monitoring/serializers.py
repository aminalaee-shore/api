from rest_framework import serializers

from .models import Alert, AlertLog, Product, ProductLog


class AlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alert
        fields = '__all__'


class AlertLogSerializer(serializers.ModelSerializer):
    alert = AlertSerializer()

    class Meta:
        model = AlertLog
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class ProductLogSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = ProductLog
        fields = '__all__'
