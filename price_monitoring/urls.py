from django.urls import include, path
from rest_framework.routers import DefaultRouter

from price_monitoring import views

router = DefaultRouter()
router.register(r'alerts/logs', views.AlertLogViewSet)
router.register(r'alerts', views.AlertViewset)
router.register(r'ebay', views.EbayStubViewset)
router.register(r'products/logs', views.ProductLogViewset)
router.register(r'products', views.ProductViewset)

urlpatterns = [
    path('', include(router.urls)),
]
