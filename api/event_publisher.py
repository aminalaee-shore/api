from walrus import Database

from .settings import REDIS_HOST

redis_db = Database(host=REDIS_HOST, port=6379, db=0)
event_publisher = redis_db.Stream('PRODUCTS')
