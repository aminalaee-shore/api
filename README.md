##  Project Structure

The application has a simple default django structure. The main application is called `price_monitoring`. I've made use of `Dramatiq` (Celery alternative) to run background tasks like getting ebay data, sending email and etc.

The database is `PostgreSQL` and `Redis` is used as broker for Dramatiq and also for communicating events to the other application.

For event driven architecture I've used simple `Walrus` library and Redis `Stream` objects to send and receive data in a simple manner. For more complex setups I'd use Rabbitmq.

Ebay suspended my accoutns so I created a stub endpoint and mocked ebay response with random data.

There's a scripts folder which implements [Github Scripts to rule them all](https://github.com/github/scripts-to-rule-them-all) so testing and linting code is done via scripts and can be used in CI/CD.

The swagger is auto-generated using `drf_spectacular` and available on `/schema/swagger-ui`
