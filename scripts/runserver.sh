#!/usr/bin/env ash

set -e
set -x

python manage.py migrate
python manage.py runserver
# gunicorn api.wsgi -b 0.0.0.0:8000
