#!/usr/bin/env bash

set -e
set -x

bash ./scripts/lint.sh

python manage.py test
# coverage report --fail-under 98
# coverage xml
