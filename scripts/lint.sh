#!/usr/bin/env bash

set -e
set -x

isort -c api price_monitoring
pylint api price_monitoring
mypy api price_monitoring
