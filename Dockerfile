FROM python:3.9-alpine AS builder
RUN apk add --no-cache postgresql-dev gcc python3-dev musl-dev linux-headers
COPY requirements.txt .
RUN pip install -r requirements.txt


FROM python:3.9-alpine
ENV PYTHONUNBUFFERED=1 PYTHONDONTWRITEBYTECODE=1
RUN apk add --no-cache libpq
COPY --from=builder /usr/local/lib/ /usr/local/lib/
WORKDIR /app
COPY . .
CMD ["./scripts/runserver.sh"]
